IOP Planner
===========

Installation
************

1. Setup virtualenv
2. python setup.py develop
3. Download minizinc:
```
wget https://github.com/MiniZinc/MiniZincIDE/releases/download/2.0.14/MiniZincIDE-2.0.14-bundle-linux-x86_64.tgz
```
4. Unpack it
```
atool -x MiniZincIDE-2.0.14-bundle-linux-x86_64.tgz
```
5. Change 'minizinc_base_path' variable in main.py to reflect your Minizinc location
6. Run main.py from parent directory
