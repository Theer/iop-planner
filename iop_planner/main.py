import os
import traceback
from copy import copy, deepcopy
from datetime import timedelta, datetime
from itertools import islice

import pymzn

from iop_planner.config import MINIZINC_BASE_PATH
from iop_planner.post_processing import post_process_result
from iop_planner.solver import g12lazy, run_solver
from iop_planner.solver.minizinc_service import run_solver_lns, normalize_schedule
from iop_planner.test_cases.cases import TEST_CASES
from iop_planner.utils import print_schedule, print_statistics


def configure_pymzn():
    pymzn.config.mzn2fzn_cmd = os.path.join(MINIZINC_BASE_PATH, "mzn2fzn")
    pymzn.config.solns2out_cmd = os.path.join(MINIZINC_BASE_PATH, "solns2out")
    pymzn.config.gecode_cmd = os.path.join(MINIZINC_BASE_PATH, "fzn-gecode")
    pymzn.config.cmd_arg_limit = 1


def get_test_case_from_day_to_day(test_case, week):
    week_test_case = deepcopy(test_case)
    week_test_case['end_date'] = week_test_case['start_date'] + timedelta(days=6 + week*7)
    week_test_case['last_day_of_the_month'] = week_test_case['end_date']
    week_test_case['regular_employees_count'] = week_test_case['regular_employees_count'][:(week+1)*7]
    week_test_case['prototype_schedule'] = list(zip(*list(zip(*week_test_case['prototype_schedule']))[:(week+1)*28]))
    return week_test_case

# def get_test_case_from_day_to_day(test_case, week):
#     week_test_case = deepcopy(test_case)
#     week_test_case['end_date'] = week_test_case['start_date'] + timedelta(days=6 + week*7)
#     week_test_case['last_day_of_the_month'] = week_test_case['end_date']
#     week_test_case['regular_employees_count'] = week_test_case['regular_employees_count'][:(week+1)*7]
#     week_test_case['prototype_schedule'] = list(zip(*list(zip(*week_test_case['prototype_schedule']))[:(week+1)*28]))
#     return week_test_case


def get_whole_schedule_test_case(original_test_case, schedule):
    whole_test_case = deepcopy(original_test_case)
    whole_test_case['start_date'] = original_test_case['start_date']
    whole_test_case['first_day_of_the_month'] = original_test_case['first_day_of_the_month']
    shifts = len(schedule[0])
    whole_test_case['end_date'] = whole_test_case['start_date'] + timedelta(days=shifts / 4 - 1)
    whole_test_case['last_day_of_the_month'] = whole_test_case['end_date']
    whole_test_case['regular_employees_count'] = whole_test_case['regular_employees_count'][:int(shifts/4)]
    whole_test_case['prototype_schedule'] = list(map(lambda x: list(map(lambda y: y and 2 or 1, x)), schedule))
    return whole_test_case

def run_test_cases(test_name=None):
    tests = TEST_CASES.items()
    if test_name:
        tests = [t for t in tests if t[0] == test_name]

    for test_case_name, test_case in tests:
        print("\n" + test_case_name)
        for prop in ['babysitters_count', 'start_date', 'end_date',
                     'kids_count', 'nurses_count']:
            print(prop + ': ' + str(test_case[prop]))

        schedule = run_solver(**test_case, debug=True, solver=g12lazy, timeout=30000)
        if schedule:
            print_statistics(schedule, test_case['kids_count'])
            post_processed_schedule = post_process_result(schedule)
            print_schedule(post_processed_schedule)
        else:
            print("Problem has not been solved, most likely due to timeout.")



def run_test_cases_weekly(test_name=None):
    tests = TEST_CASES.items()
    if test_name:
        tests = [t for t in tests if t[0] == test_name]

    for test_case_name, test_case in tests:
        print("\n" + test_case_name)
        for prop in ['babysitters_count', 'start_date', 'end_date',
                     'kids_count', 'nurses_count']:
            print(prop + ': ' + str(test_case[prop]))

        weeks_count = int((test_case['end_date'] - test_case['start_date'] + timedelta(days=1)).days/7)
        week_schedules = []
        for week in range(weeks_count):
            week_test_case = get_test_case_from_day_to_day(test_case, week)

            start = datetime.now()
            schedule = run_solver_lns(**week_test_case, debug=True, solver=g12lazy, timeout=30000)
            end = datetime.now()

            print('Calculated %i week in %i seconds' % (week, (end-start).seconds))

            week_schedules = list(zip(*schedule))
            prototype_schedule = list(zip(*test_case['prototype_schedule']))
            prototype_schedule[0:28+week*28] = week_schedules
            test_case['prototype_schedule'] = list(zip(*prototype_schedule))

        schedule = list(zip(*week_schedules))
        schedule = normalize_schedule(schedule)
        if schedule:
            print_statistics(schedule, test_case['kids_count'])
            post_processed_schedule = post_process_result(schedule)
            print_schedule(post_processed_schedule)

            whole_schedule = get_whole_schedule_test_case(test_case, schedule)
            schedule = run_solver(**whole_schedule, debug=True, solver=g12lazy, timeout=30000)
            print("Generated schedule is correct")
        else:
            print("Problem has not been solved, most likely due to timeout.")


def main():
    try:
        configure_pymzn()
        run_test_cases_weekly("EASY_CASE")
    # run_test_cases("EASY_CASE_HARDCODED")
    # run_test_cases("REAL_LIFE_PROBLEM")
    except:
        traceback.print_exc()


if __name__ == "__main__":
    main()
