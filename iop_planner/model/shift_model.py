from enum import Enum


class ShiftType(Enum):
    Morning = 1
    Afternoon = 2
    Day = 3
    Night = 4
    DayNight = 5
    AfternoonNight = 6
    Absent = 7
    MagicNight = 8