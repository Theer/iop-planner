import os
import sys

DZN_FILE_PATH = os.path.join(os.path.dirname(sys.modules[__name__].__file__),
                             "model", "iop_model.dzn")

MZN_FILE_PATH = os.path.join(os.path.dirname(sys.modules[__name__].__file__),
                             "model", "iop_model_min.mzn")

MZN_SATISFY_FILE_PATH = os.path.join(os.path.dirname(sys.modules[__name__].__file__),
                             "model", "iop_model.mzn")

MINIZINC_BASE_PATH = "~/Apps/minizinc/"
