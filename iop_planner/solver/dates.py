from datetime import date, datetime, timedelta


def datespan(start_date, end_date, delta=timedelta(days=1)):
    current_date = start_date
    while current_date <= end_date:
        yield current_date
        current_date += delta


def easter_of_the_year(year):
    """
    Implementation of the so called Butcher's algorithm
    to determine the date of the Easter for a given year
    """

    a = year % 19
    b = year // 100
    c = year % 100
    d = (19 * a + b - b // 4 - ((b - (b + 8) // 25 + 1) // 3) + 15) % 30
    e = (32 + 2 * (b % 4) + 2 * (c // 4) - d - (c % 4)) % 7
    f = d + e - 7 * ((a + 11 * d + 22 * e) // 451) + 114
    month = f // 31
    day = f % 31 + 1
    return date(year, month, day)


def list_public_holidays(start_year, end_year):
    REGULAR_HOLIDAYS = [
            (1, 1), # Nowy Rok
            (6, 1), # Trzech Kroli
            (1, 5), # Pierwszy Maja
            (3, 5), # Trzeci Maja
            (15, 8), # Swieto Wojska Polskiego
            (1, 11), # Wszystkich Swietych
            (11, 11), # Swieto Niepodleglosci
            (25, 12), # Boze Narodzenie
            (26, 12), # drugi dzien Bozego Narodzenia
    ]

    years = [year for year in range(start_year, end_year+1)]

    public_holidays = [date(year, regular_holiday[1], regular_holiday[0])
                       for year in years for regular_holiday in REGULAR_HOLIDAYS]

    for year in years:
        easter = easter_of_the_year(year)
        easter_monday = easter + timedelta(1)
        pentacost = easter + timedelta(49)
        corpus_cristi = easter + timedelta(60)
        public_holidays += [easter, easter_monday, pentacost, corpus_cristi]

    return public_holidays


def how_many_workdays(start_date, end_date):
    SATURDAY = 5
    public_holidays = list_public_holidays(start_date.year, end_date.year)

    workdays = [d for d in datespan(start_date, end_date)
                if d.weekday() < SATURDAY and d not in public_holidays]

    return len(workdays)
