import os
from copy import copy, deepcopy
from datetime import date, datetime, timedelta
from math import ceil

import sys
from random import randint

import pymzn

from iop_planner.config import (
        DZN_FILE_PATH,
        MINIZINC_BASE_PATH,
        MZN_FILE_PATH,
        MZN_SATISFY_FILE_PATH)
from iop_planner.solver.g12 import G12LazySolver
from iop_planner.solver.dates import datespan, how_many_workdays

NURSE_PRESENT_DURING_SHIFT = 2


def normalize_schedule(schedule):
    '''
    Wynik z Minizinc zawiera 1 gdy pielegniarka jest nieobecna
    a 2 gdy jest obecna w pracy. Dlatego tutaj dla porzadku
    odejmujemy 1
    :param schedule:
    :return:
    '''
    new_schedule = []
    for s in schedule:
        new_schedule.append(
                list(map(lambda x: x == NURSE_PRESENT_DURING_SHIFT, s)))
    return new_schedule


def make_empty_prototype_schedule(days_count, how_many_employees):
    return [[0] * (4 * days_count)] * (how_many_employees)


def datespan(start_date, end_date, delta=timedelta(days=1)):
    current_date = start_date
    while current_date <= end_date:
        yield current_date
        current_date += delta


def worktime_per_employee(hours_per_week,
                          private_holidays_count, workdays_count):
    """
    Oblicza liczbe godzin do przepracowania przez danego pracownika
    w danym okresie rozliczeniowym przy okreslonym etacie
    (wyrazonym w godzinach na tydzien)
    """


    # TODO: take into consideration the effect of ceil in post-processing
    return ceil(
            (hours_per_week * (workdays_count - private_holidays_count)) / 5
            )


def get_work_time(nurses_and_babysitters_hours_per_week,
                  nurses_and_babysitters_private_holidays,
                  start_date, end_date):
    """
    Zwraca jako liste liczbe godzin do przepracowania w podanym zakresie
    przez kazdego pracownika.

    :param nurses_and_babysitters_hours_per_week: lista zawierajaca liczbe godzin,
                                    jaka przepracowuje pracownik tygodniowo
                                    (innymi slowy, to wymiar etatu)
    :param nurses_and_babysitters_private_holidays: lista zawierajaca liczbe
                                        dni wolnych, podjeta przez danego
                                        pracownika w okreslonym okresie
                                        (wakacje, chorobowy etc.)
    :param start_date: pierwszy dzien danego okresu
    :param end_date: ostatni dzien danego okresu

    """

    if len(nurses_and_babysitters_hours_per_week) != len(nurses_and_babysitters_private_holidays):
        raise TypeError("nurses_and_babysitters_hours_per_week \
                        and nurses_and_babysitters_private_holidays \
                        of different length.")

    workdays_count = how_many_workdays(start_date, end_date)

    return [worktime_per_employee(hours_per_week, private_holidays_count,
                                  workdays_count)
            for hours_per_week, private_holidays_count in zip(
                    nurses_and_babysitters_hours_per_week,
                    nurses_and_babysitters_private_holidays)]


def print_raw_minizinc_result(raw_result):
    schedule = raw_result['schedule']
    overtime = raw_result['overtime']
    week_nurses_hours_sum = raw_result['nurses_week_hours_sum']
    nurses_months_hours_sum = raw_result['nurses_months_hours_sum']


    print('Overtime (hours):', overtime)
    print('Week (hours):', week_nurses_hours_sum)
    print('Month (hours):', nurses_months_hours_sum)
    for nurse_schedule in schedule:
        print('|', ' '.join(map(str, nurse_schedule)), '|')

def run_solver_lns(*args, **kwargs):
    best_schedule, overtime_list = run_solver(*args, **kwargs, mzn_file=MZN_SATISFY_FILE_PATH)
    best_overtime = sum(overtime_list)
    kwargs['prototype_schedule'] = deepcopy(best_schedule)
    for i in range(200):
        random_nurse_1 = randint(0, len(kwargs['prototype_schedule']) - 1)
        random_nurse_2 = randint(0, len(kwargs['prototype_schedule']) - 1)
        random_nurse_3 = randint(0, len(kwargs['prototype_schedule']) - 1)
        # random_nurse_4 = randint(0, len(kwargs['prototype_schedule']) - 1)
        # random_nurse_5 = randint(0, len(kwargs['prototype_schedule']) - 1)
        # random_nurse_6 = randint(0, len(kwargs['prototype_schedule']) - 1)

        shifts = 28
        kwargs['prototype_schedule'][random_nurse_1][-28:] = [0]*shifts
        kwargs['prototype_schedule'][random_nurse_2][-28:] = [0]*shifts
        kwargs['prototype_schedule'][random_nurse_3][-28:] = [0]*shifts
        # kwargs['prototype_schedule'][random_nurse_4][-28:] = [0]*shifts
        # kwargs['prototype_schedule'][random_nurse_5][-28:] = [0]*shifts
        # kwargs['prototype_schedule'][random_nurse_6][-28:] = [0]*shifts

        schedule, overtime_list = run_solver(*args, **kwargs)
        new_overtime_sum = sum(overtime_list)
        print("Found solution with overtime: %i" % new_overtime_sum)
        if new_overtime_sum < best_overtime:
            best_overtime = new_overtime_sum
            best_schedule = deepcopy(schedule)

        kwargs['prototype_schedule'] = deepcopy(best_schedule)

    return best_schedule


def run_solver(start_date, end_date,
               first_day_of_the_month, last_day_of_the_month,
               nurses_and_babysitters_hours_per_week,
               nurses_and_babysitters_private_holidays,
               nurses_count, babysitters_count,
               regular_employees_count, kids_count,
               attitude_towards_overtime,
               prototype_schedule=None, debug=False,
               solver=None, timeout=0, mzn_file=MZN_FILE_PATH):


    days = list(datespan(start_date, end_date))
    days_count = len(days)

    first_day_of_the_month_index = days.index(first_day_of_the_month) + 1
    last_day_of_the_month_index = days.index(last_day_of_the_month) + 1
    work_time = get_work_time(nurses_and_babysitters_hours_per_week,
                              nurses_and_babysitters_private_holidays,
                              start_date, end_date)

    data = {
            'days_count': days_count,
            'first_day_of_the_month_index': first_day_of_the_month_index,
            'last_day_of_the_month_index': last_day_of_the_month_index,
            'attitude_towards_overtime': attitude_towards_overtime,
            'nurses_count': nurses_count,
            'babysitters_count': babysitters_count,
            'regular_employees_count': regular_employees_count,
            'kids_count': kids_count,
            'prototype_schedule': prototype_schedule,
            'work_time': work_time,
            'use_max_babysitters_constraint': False
    }

    if prototype_schedule is None:
        prototype_schedule = make_empty_prototype_schedule(
                days_count, nurses_count + babysitters_count)

    if not solver:
        solver = pymzn.gecode

    if isinstance(solver, G12LazySolver):
        result, complete = solver.solve(mzn_file=mzn_file,
                                        dzn_file=DZN_FILE_PATH,
                                        data=data, check_complete=True,
                                        timeout=timeout)
    else:
        result, complete = pymzn.minizinc(mzn_file,
                                          DZN_FILE_PATH,
                                          data=data, solver=solver,
                                          timeout=timeout, check_complete=True)
        result = result[0]

    if complete:
        print(result)
        if debug:
            print_raw_minizinc_result(result)
        return result['schedule'], result['overtime']

    else:
        return []
