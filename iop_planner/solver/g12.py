from subprocess import CalledProcessError
from tempfile import NamedTemporaryFile

from pymzn import dzn, MiniZincUnsatisfiableError, Solver
from pymzn.bin import run

from iop_planner.solver.utils import deserialize_output_stream

class G12LazySolver(Solver):
    def __init__(self, path=None):
        self.path = path
        self.cmd = path or 'mzn-g12lazy'
        super().__init__(False, True, globals_dir=None)

    def solve(self, mzn_file, *, dzn_file=None, data=None,
              check_complete=False, all_solutions=False,
              timeout=None, parallel=1, n_solns=-1, seed=0, **kwargs):
        """Solves a problem with the g12-lazy solver.
        Parameters
        ----------
        mzn_file : str
            The path to the mzn file to use as input of the solver.
        dzn_file : str
            The path to the dzn file to use as data input of the solver.
        data : str
            A dictionary of data input of the solver.
        all_solutions : bool
            Whether the solver should output all the solutions. Equivalent to
            ``n_solns=0``.
        check_complete : bool
            Whether the solver should return a second boolean value indicating
            if the search was completed successfully.
        n_solns : int
            The number of solutions to output (0 = all, -1 = one/best);
            the default is -1.
        parallel : int
            The number of threads to use to solve the problem
            (0 = #processing units); default is 1.
        time : int or float
            The time cutoff in seconds, after which the execution is truncated
            and the best solution so far is returned, 0 means no time cutoff;
            default is 0.
        seed : int
            Random seed; default is 0.

        Returns
        -------
        str or tuple
            A string containing the solution output stream of the execution of
            g12-lazy on the specified problem; it can be directly be given to the
            function solns2out to be evaluated. If ``check_complete=True``
            returns an additional boolean, checking whether the search was
            completed before the timeout.
        """

        args = [self.cmd]
        # args.extend(["--solver", "/home/theer/Apps/minizinc/mzn-g12lazy"])
        # args.append("-v")
        if n_solns > 0:
            args.append('-n')
            args.append(n_solns)
        elif n_solns == 0 or all_solutions:
            args.append('-a')
        if parallel != 1:
            args.append('-p')
            args.append(str(parallel))
        if seed != 0:
            args.append('-r')
            args.append(seed)
        if dzn_file:
            data_file = NamedTemporaryFile(prefix='pymzn_', suffix='.dzn',
                                           delete=False, mode='w+',
                                           buffering=1)
            with data_file:
                with open(dzn_file, 'r') as dzn_file:
                    data_file.write(dzn_file.read())
                if data:
                    data_elements = dzn(data)
                    for dat_el in data_elements:
                        data_file.write('{}\n\n'.format(dat_el))

            args.append('-d')
            args.append(data_file.name)

        args.append(mzn_file)

        try:
            process = run(args, timeout=timeout)
            complete = not process.expired
            out = process.stdout
            error = process.stderr
        except CalledProcessError as err:
            raise RuntimeError(err.stderr) from err

        if 'UNSATISFIABLE' in out:
            print(out)
            print(error)
            raise MiniZincUnsatisfiableError()

        out = deserialize_output_stream(out)
        if check_complete:
            return out, complete
        return out


#: Default G12LazySolver instance.
g12lazy = G12LazySolver()
# g12lazy = G12LazySolver(path="/home/theer/Documents/Studia/Workshop2/libminizinc/minisearch")