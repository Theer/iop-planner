__author__ = 'Lukasz Chrzaszcz'

from .minizinc_service import run_solver
from .g12 import g12lazy

__all__ = ['run_solver', 'g12lazy',]
