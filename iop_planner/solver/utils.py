import ast
import operator
import re

from functools import reduce

NUMBER_PATTERN = re.compile("[-]*[0-9].*\.[0-9]*")


def deserialize_output_stream(stream):
    """Converts output stream from a solver (esp. g12lazy) into Python dict
    """

    out = {}
    element_strings = stream.split(';\n')

    for element_string in element_strings:
        if ' = ' in element_string:
            var_name = element_string.split(' = ')[0]
            variable_val_string = element_string.split(' = ')[1]

            if var_name in out:
                raise RuntimeError('Probably second solution '
                                   'from the same stream is being parsed. '
                                   'This has to be implemented.')
            
            out[var_name] = variable_val_string_to_value(variable_val_string)

    return out


def _is_a_number(string):
    return NUMBER_PATTERN.match(string)


def variable_val_string_to_value(variable_val_string):
    if 'array' in variable_val_string:
        return parse_array_string(variable_val_string)
    elif 'set' in variable_val_string or '..' in variable_val_string_to_value:
        #TODO: implement this
        raise NotImplementedError('Parsing sets not implemented yet')
    elif variable_val_string == 'true':
        return True
    elif variable_val_string == 'false':
        return False
    elif _is_a_number(variable_val_string):
        if all(char.isdigit() for char in variable_val_string):
            return int(variable_val_string)
        else:
            return float(variable_val_string)
    else:
        return variable_val_string


def parse_array_string(array_string):
    """Parse string representing an array in Minizinc style
    to make it a Python list or list of lists or list of lists of lists... etc.
    """

    dimensions_substrings = re.search(
            '\((.*)\[', array_string).group(1).split(' ,')[:-1]

    dimension_sizes = []

    for dimensions_substring in dimensions_substrings:
        start, end = tuple(map(int, dimensions_substring.split('..')))
        dim_size = end-start + 1
        if dim_size > 1:
            dimension_sizes.append(end-start + 1)

    flat_array_string = re.search('\[.*\]', array_string).group(0)
    flat_array = ast.literal_eval(flat_array_string)

    return unflatten_array(flat_array, dimension_sizes)


def unflatten_array(flat, dims):
    subdims = dims[1:]
    subsize = reduce(operator.mul, subdims, 1)
    if dims[0]*subsize!=len(flat):
        raise ValueError("Size does not match or invalid")
    if not subdims:
        return flat
    return [unflatten_array(
            flat[i:i+subsize], subdims) for i in range(0,len(flat), subsize)]
