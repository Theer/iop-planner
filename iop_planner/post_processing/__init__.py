from _operator import itemgetter
from itertools import groupby

from iop_planner.model.shift_model import ShiftType

__author__ = 'Lukasz Chrzaszcz'


# TODO: take into consideration the effect of ceil in post-processing
# in minizinc_service/worktime_per_employee() function

SHIFTS = {
    (1, 0, 0, 0): ShiftType.Morning,
    (0, 1, 0, 0): ShiftType.Afternoon,
    (1, 1, 0, 0): ShiftType.Day,
    (0, 0, 1, 1): ShiftType.Night,
    (1, 1, 1, 1): ShiftType.DayNight,
    (0, 1, 1, 1): ShiftType.AfternoonNight,
    (0, 0, 0, 1): ShiftType.MagicNight,
    (0, 0, 0, 0): ShiftType.Absent
}


def translate_nurse_shift_to_days(nurse_schedule):
    """
    Translates single nurse shifts to days

    :param nurse_schedule: List of presence in work, example: [1 0 0 1 1 0]
    :return: List of day types, example: ['P', 'D']
    """
    groups = groupby(list(enumerate(nurse_schedule)), lambda x: x[0] // 4)
    grouped_days = [tuple(map(itemgetter(1), list(group))) for (key, group) in groups]
    return list(map(SHIFTS.get, grouped_days))


def translate_shifts_to_days(schedule):
    return map(translate_nurse_shift_to_days, schedule)


def post_process_result(schedule):
    return translate_shifts_to_days(schedule)
