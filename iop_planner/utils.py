from itertools import groupby

import itertools

from math import ceil

from iop_planner.model.shift_model import ShiftType
from iop_planner.post_processing import SHIFTS

SHIFT_STRING = {
    ShiftType.Morning: 'R ',
    ShiftType.Afternoon: 'P ',
    ShiftType.Day: 'D ',
    ShiftType.Night: 'N ',
    ShiftType.DayNight: 'DN',
    ShiftType.AfternoonNight: 'PN',
    ShiftType.MagicNight: 'N8',
    ShiftType.Absent: '- '
}


def split_nurse_schedule_to_weeks(nurse_schedule):
    """
    Splits single nurse schedule to weeks
    :param nurse_schedule:
    :return:
    """
    groups = groupby(list(enumerate(nurse_schedule)), lambda x: x[0] // 7)
    return [list(map(lambda x: str(x[1]), list(group))) for (key, group) in groups]


def split_schedule_to_weeks(schedule):
    return map(split_nurse_schedule_to_weeks, schedule)


def change_nurse_shift_type_to_string(nurse_schedule):
    return map(SHIFT_STRING.get, nurse_schedule)


def change_shift_type_to_string(schedule):
    return list(map(change_nurse_shift_type_to_string, schedule))


def print_schedule(schedule):
    formatted_schedule = change_shift_type_to_string(schedule)
    weekly_schedule = split_schedule_to_weeks(formatted_schedule)
    print('')
    for nurse_schedule in weekly_schedule:
        print('|', ' | '.join(map(lambda w: ' '.join(map(str, w)), nurse_schedule)), '|')


def get_shift_bits_from_string(shift_string):
    if len(shift_string) == 1:
        shift_string += " "

    shift = [k for k, v in SHIFT_STRING.items() if v == shift_string]
    shift_type = shift and shift[0] or ShiftType.Absent
    shift_bits = [k for k, v in SHIFTS.items() if shift_type == v][0]
    return list(shift_bits)


def parse_schedule(schedule):
    """
    Parses real life schedule in CSV format and returns it as shifts representation

    Example:
    RP, D, N,,
    Returns
    [1, 1, 0, 0,  1, 1, 0, 0,  0, 0, 1, 1,  0, 0, 0, 0]

    :param schedule:
    :return:
    """
    shift_bits_schedule = []
    for employee_schedule in schedule.split('\n'):
        employee_shift_bits_schedule = map(get_shift_bits_from_string, employee_schedule.split(','))
        employee_shift_bits_schedule = list(itertools.chain(*employee_shift_bits_schedule))
        shift_bits_schedule.append(employee_shift_bits_schedule)
    return shift_bits_schedule


def from_file(filepath):
    with open(filepath) as fd:
        schedule = parse_schedule(fd.read())
        return schedule


def print_statistics(schedule, kids_count):
    min_nurses_per_shift = [
        str(int(ceil(kids_count/3.0))),
        str(int(ceil(kids_count/3.0))),
        str(int(ceil(kids_count/3.0))),
        str(int(ceil(kids_count/5.0)))
    ]

    nurses_on_shift = map(lambda x: str(x.count(True)), zip(*schedule))
    required_nurses_on_shift = itertools.islice(itertools.cycle(min_nurses_per_shift), 0, len(schedule[0]))

    print ("Nurses on shift")
    print("|", " ".join(nurses_on_shift), "|")
    print ("Required nurses on shift")
    print("|", " ".join(required_nurses_on_shift), "|")
