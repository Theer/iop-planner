#!/usr/bin/env python

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='IOP Planner',
    version='1.0',
    description='Schedule planner for IOP ',
    author='AGH',
    author_email='',
    url='',
    packages=['iop_planner'],
    install_requires=['pymzn'],
    entry_points={
        'console_scripts': [
            'iop_planner = iop_planner.main:main'
        ]
    }
)
